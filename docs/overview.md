# Overview of the IoT kit

Your IoT Kit contains the items listed below. These items have been chosen such
that students can get a wide variety of experience with multiple types of
hardware. The hardware is hence representative of the diversity of hardware that
is available in the market today. This kit also aims to demonstrate the scale of
experiments and work that can be done with readily available hardware and at
such a low cost.

## Common Components

### Breadboards

### Jumper Wires

### Soldering Irons

### Wire Strippers

###  Multimeter

## Power Supplies

### Breadboard Power Supply

### USB Power Supply

### 12V Power Supply

### Power Strips

### LiPo Batteries

## Sensors

### Pulse Sensor
### Water Sensor
### Current Sensor
### Non-Invasive Current Sensor
### Temperature (Analog)
### Temperature (Digital)
### Humidity
### Simple LED
### RGB LED
### Switch
### Hall Magnetic
### Reed Relay
### Relay
### Photo Resistor
### Joystick

## Microcontrollers

### Arduino Mega
### Arduino Pro Micro
### Digispark
### NodeMCU
### Wemos D1 Mini

## Single Board Computers

### Raspberry Pi 3
### Raspberry Pi Zero
### Onion Omega2
### Orange Pi 2G IOT
### Nano Pi Neo Air

## LEDs and Displays

### RGB LED Strip
### OLED Displays
### OrangePi 2G LCDs
### 7" HDMI LCDs

## Specialised Boards

### HealthyPi

