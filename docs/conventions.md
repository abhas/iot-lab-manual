# Conventions used in this manual

## Use of Markdown

Complete content for this manual is written in Markdown with extensions
provided by
[mkdocs-material](https://squidfunk.github.io/mkdocs-material/extensions/admonition/)
and [PyMarkdown](https://facelessuser.github.io/pymdown-extensions/).
Using Markdown makes the entire system of writing content extremely
simple with no overhead of having to resort to using HTML or CSS tricks
to format the content.

## Fonts

The main content is formatted using a Sans Serif font. Code, filenames,
directory names and so on are formatted using monospace fonts for
accuracy and attention.

## Notes, Tips and Warnings

### Note blocks

!!! note
    This is a note and it is used to highlight certain aspects of the
    content. It can contain just plain content or also code blocks.

### Summary blocks

!!! summary
    This is a Summary block and it is used in places to summarize a
    portion of the content.

### Tip blocks

!!! tip
    Tip blocks, like this, provide a way to give pointers to other
    things that enhancements and other ways of doing things or tricks /
    hacks that can help you.

### Warning blocks

!!! warning
    Warning blocks are used to provide visual warnings and highlights
    for things that you must be careful about or pay extra attention to.

### Danger blocks

!!! danger
    Danger blocks are like warning blocks but are used for warnings that
    might cause things to malfunction or stop working or might cause
    potential harm or damage.

### Quote blocks

!!! quote
    Quote blocks are used to present quotations from other documentation
    sources or books or videos and so on. They also contain attribution
    details so that you know where the quote has been taken from.
